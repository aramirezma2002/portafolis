---
title: "Mis proyectos"
---

- [La Cantina de Prodis](https://github.com/aramirezma2002/LaCantinaDeProdis)

La Cantina de Prodis es una aplicación que desarrollemos para que la gente de la fundación Prodis pueda hacer pedidos remotos a su cantina. 

- [Candy&Co. ERP](https://github.com/aramirezma2002/Candy-CoERP)

El ERP de Candy&Co. es una ERP para gestionar los almacenes de chuches de una empresa ficticia.

- [Five To Survive](https://github.com/aramirezma2002/Five-to-Survive)

Five To Survive es un juego hecho con Unity donde tienes que sobrevivir cinco rondas para ganar.

- [GExpenses](https://github.com/aramirezma2002/GExpenses.git)

GExpenses es una aplicación para gestionar los gastos de actividades.

- [CraftMade](https://github.com/jcadafalch/Marketplace)

CraftMade es un Marketplace donde se venden productos de artesanía.
---
title: "My projects"
---

- [La Cantina de prodis](https://github.com/aramirezma2002/LaCantinaDeProdis)   

La Cantina de Prodis is a mobile application that we developed so that the people of the Prodis foundation can place remote orders at their canteen.

- [Candy&Co. ERP](https://github.com/aramirezma2002/Candy-CoERP)

The ERP of Candy&Co. is an ERP to manage the candy stores of a fictitious company.

- [Five To Survive](https://github.com/aramirezma2002/Five-to-Survive)

Five To Survive is a game made with Unity where you have to survive five rounds to win.

- [GExpenses](https://github.com/aramirezma2002/GExpenses.git)

GExpenses is an application to manage activity expenses.

- [CraftMade](https://github.com/jcadafalch/Marketplace)

CraftMade is a Marketplace where craft products are sold.
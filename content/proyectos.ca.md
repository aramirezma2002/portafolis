---
title: "Els meus projectes"
---

- [La Cantina de prodis](https://github.com/aramirezma2002/LaCantinaDeProdis)

La Cantina de Prodis és una aplicació que vam desenvolupar perquè la gent de la fundació Prodis pugui fer comandes remotes a la seva cantina.

- [Candy&Co. ERP](https://github.com/aramirezma2002/Candy-CoERP)

L'ERP de Candy&Co. és una ERP per gestionar els magatzems de llaminadures d'una empresa fictícia.

- [Five To Survive](https://github.com/aramirezma2002/Five-to-Survive)

Five To Survive és un joc fet amb Unity on has de sobreviure cinc onades per guanyar.

- [GExpenses](https://github.com/aramirezma2002/GExpenses.git)

GExpenses és una aplicació per gestionar les despeses d'activitats.

- [CraftMade](https://github.com/jcadafalch/Marketplace)

CraftMade es un Marketplace on es venen productes artesanals.